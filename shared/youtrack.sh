#!/bin/sh

SERVICE_NAME=YouTrack
YOUTRACK_DIR=/usr/local/youtrack/
YOUTRACK_JAR=youtrack.jar
PID_FILE=/var/run/youtrack.pid
RUN_PORT=80

startd() {
    echo "Starting $SERVICE_NAME ..."
    if [ ! -f $PID_FILE ]; then
        cd $YOUTRACK_DIR
        nohup java -Ddatabase.location=$YOUTRACK_DIR/teamsysdata \
                  -jar $YOUTRACK_JAR $RUN_PORT \
                  >> $YOUTRACK_DIR/console.out 2>&1 \
                  & echo $! > $PID_FILE
        echo "$SERVICE_NAME started."
    else
        echo "$SERVICE_NAME is already running."
    fi
}

stopd() {
    if [ -f $PID_FILE ]; then
        PID=$(cat $PID_FILE);
        echo "Stopping $SERVICE_NAME ..."
        kill $PID;
        echo "$SERVICE_NAME stopped."
        rm $PID_FILE
    else
        echo "$SERVICE_NAME is not running."
    fi
}

status() {
    if [ -f $PID_FILE ]; then
        PID=$(cat $PID_FILE);
        echo "$SERVICE_NAME is running (pid $PID)"
    else
        echo "$SERVICE_NAME is not running"
    fi
}

case "$1" in
  start) startd
    ;;
  stop) stopd
    ;;
  restart|reload|force-reload) stopd && startd
    ;;
  status) status
    ;;
  *) echo "Usage: $0 {start|stop|reload|force-reload|restart|status}"
     exit 1
   ;;
esac

exit 0 

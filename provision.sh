#!/usr/bin/env bash

if [ -f "/var/vagrant_provision" ]; then 
    exit 0
fi

apt-get update
apt-get install -y curl git-core unzip vim
apt-get install -y default-jre
mkdir -p /usr/local/youtrack
cp /vagrant/shared/youtrack.jar /usr/local/youtrack/
cp /vagrant/shared/youtrack.sh /etc/init.d/youtrack
chmod +x /etc/init.d/youtrack
service youtrack start
update-rc.d youtrack defaults

touch /var/vagrant_provision